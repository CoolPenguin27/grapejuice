import json
import os

from grapejuice_common.util.environment_modificication import EnvironmentModification
from grapejuice_common.util.string_util import random_alphanumeric_string


def _display_dict(d):
    print(json.dumps({**d}, indent=2, default=repr))


def test_sanitization_removes_variables(random_wineprefix):
    environment_variables = {
        random_alphanumeric_string(16): random_alphanumeric_string(16)
    }

    with EnvironmentModification(environment_variables):
        _display_dict(os.environ)
        random_wineprefix.configuration.sanitize_environment = True
        random_wineprefix.configuration.env_passthrough = []

        env = random_wineprefix.core_control.make_env()
        _display_dict(env)

        for k in environment_variables.keys():
            assert k not in env


def test_sanitization_keeps_passthrough_variables(random_wineprefix):
    environment_variables = {}

    assert len(random_wineprefix.configuration.env_passthrough) > 0
    for k in random_wineprefix.configuration.env_passthrough:
        environment_variables[k] = random_alphanumeric_string(16)

    with EnvironmentModification(environment_variables):
        _display_dict(os.environ)
        random_wineprefix.configuration.sanitize_environment = True

        env = random_wineprefix.core_control.make_env()
        _display_dict(env)

        for k in environment_variables.keys():
            assert k in env
            assert env[k] == environment_variables[k]


def test_no_sanitization_when_option_disabled(random_wineprefix):
    environment_variables = {random_alphanumeric_string(16): random_alphanumeric_string(16) for _ in range(16)}

    with EnvironmentModification(environment_variables):
        snapshot = {**os.environ}
        random_wineprefix.configuration.sanitize_environment = False

        env = random_wineprefix.core_control.make_env()
        _display_dict(env)

        for k, v in snapshot.items():
            assert k in env
            assert v == env[k]
